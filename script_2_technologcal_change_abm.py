#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""" Example script (2). ABM of a model of technological change with innovative 
    and imitative research inspired by Nelson and Winter's model. Rare large 
    innovations push the technology frontier (innovative research). Frequent 
    small improvements copy technologies already in use by other firms and help
    spread technologies in the system (imitative research).
    
    The script assumes a one-dimensional technology fully characterized by the
    unit production cost. The model is limited to one sector. The model assumes
    a fully connected network (implicitly). The model allows for firm entry and
    exit. 
    The script exposes a number of Simulation parameters.
        
@author: Torsten Heinrich
"""

import matplotlib.pyplot as plt
import numpy as np

""" Agent class. Contains an intependent decision maker within a simulation"""
class Agent():
    def __init__(self, S, i, agent_parameters, initial_cost):
        """
        Constructor method

        Parameters
        ----------
        ----------
        S : Simulation object
            The simulation the agent belongs to.
        i : int
            Unique ID number of the agent.
        agent_parameters : dict
            Simulation parameters the simulation shares with its agents. 
            Contains innovation_cost, imitation_cost, innovation_probability, 
            imitation_probability, maximum_innovation_size, dividend_share, 
            innovation_success_investment
        initial_cost : float
            Initiat technology level (in terms of unit production cost).

        Returns
        -------
        None.

        """
        """ Record parameters (that will not change)"""
        self.S = S
        self.agent_id = i
        self.money = agent_parameters["agent_initial_capital"] 
        self.innovation_cost = agent_parameters["innovation_cost"] 
        self.imitation_cost = agent_parameters["imitation_cost"] 
        self.innovation_probability = agent_parameters["innovation_probability"] 
        self.imitation_probability = agent_parameters["imitation_probability"] 
        self.innovation_max = agent_parameters["maximum_innovation_size"] 
        self.dividend_share = agent_parameters["dividend_share"] 
        self.innovation_success_investment = agent_parameters["innovation_success_investment"] 
        
        """ Initialize state variables (that can change)"""
        """ Technology in terms of unit production cost"""
        self.cost = initial_cost
        """ Production capacity"""
        self.capacity = 0
        """ Flag for bankruptcy and being forced to exit the market"""
        self.exiting = False
        
        """ Randomly (uniform) select the agent's innovation strategy"""
        """ Should the agent engage in innovative research?"""
        self.innovation_strategy = np.random.choice([0, 1])
        """ Should the agent engage in imitative research (copying 
            technologies from other firms)?"""
        self.imitation_strategy = np.random.choice([0, 1])
        
    def research(self):
        """
        Method for organizing (innovative and imitative) research, allocating
        funds and collecting the reward (additional investment) in case of
        success.

        Returns
        -------
        None.

        """
        
        """ Indicator variable"""
        innovation_success = False
        
        """ Innovative research"""
        if self.innovation_strategy:
            if self.money - self.innovation_cost < 0:
                """ Agent defaults and exits when they are out of money"""
                self.exiting = True
            else:
                """ Agent pays for research"""
                self.money -= self.innovation_cost
                success = np.random.random()
                """ Agent is successful with fixed probability"""
                if success < self.innovation_probability:
                    """ Innovative research grants a random improvement of the
                        unit production costs if successful. 
                        1-self.innovation_max is between 0 and 1, hence
                        innovation_size is between 0 and 1 and self.cost is
                        decreased."""
                    innovation_success = True
                    innovation_size = np.random.uniform(1 - self.innovation_max, 1)
                    self.cost *= innovation_size
                                    
        """ Imitative research"""
        if self.imitation_strategy:
            if self.money - self.imitation_cost < 0:
                """ Agent defaults and exits when they are out of money"""
                self.exiting = True
            else:
                """ Agent pays for research"""
                self.money -= self.imitation_cost
                """ Agent is successful with fixed probability"""
                success = np.random.random()
                if success < self.imitation_probability:
                    """ Imitative research grants a random technology currently
                        in use by another agent. If this technology is better
                        than the one the present agent presently uses, their
                        unit production cost decreases; else the cost and 
                        technology stay as they are. The agent will still
                        collect additional investment, as the investors are 
                        assumed to be unaware of whether the new technology 
                        is better."""
                    innovation_success = True
                    new_tech = self.S.imitation()
                    if new_tech < self.cost:
                        self.cost = new_tech
        
        """ Allocate remaining funds to production"""
        self.capacity = self.money / self.cost * (1 - self.dividend_share)
        
        """ If the agent is out of money and has not had any new innovation, 
            they must exit the market."""
        if (self.capacity == 0) and (innovation_success == False):
            self.exiting = True
        
        """ Reset agent funding. Agent will now potentially receive investments 
            and revenues"""
        self.money = 0
        
        """ Receive additional investment in case of research success. This is
            also important so the agent can in each case use the new technology 
            and is not immediately driven out if they are out of money."""
        if innovation_success:
            self.money += self.innovation_success_investment
            
    def offer(self):
        """
        Getter method for current capacity and technology

        Returns
        -------
        numeric
            Production capacity.
        numeric
            Technology (unit production cost).

        """
        return self.capacity, self.cost
        
    def obtain_revenue(self, revenue):
        """
        Method for collecting revenue.

        Parameters
        ----------
        revenue : numeric
            Revenue the agent collects.

        Returns
        -------
        None.

        """
        self.money += revenue
    
    def exit(self):
        """
        Getter method for bankrupt/exiting status.

        Returns
        -------
        bool
            Exiting status.

        """
        return self.exiting
    
    def __del__(self):
        """
        Destructor method.

        Returns
        -------
        None.

        """
        print("Firm ", self.agent_id, "exiting")


""" Simulation class. Contains the entire run of one simulation for one 
    parameter setting.
"""
class Simulation():
    def __init__(self, 
                 n_agents=10, 
                 t_max=5000, 
                 demand_budget_per_period=1000000, 
                 entry_rate=0.5, 
                 initial_unit_cost=10,
                 innovation_probability=0.08,
                 imitation_probability=0.5,
                 max_innovation_size=0.01,
                 dividend_share=0.02):
        """
        Constructor method.

        Parameters
        ----------
        n_agents : int, optional
            Initial number of agents. The default is 10.
        t_max : int, optional
            Runtime of the simulation. The default is 5000.
        demand_budget_per_period : numeric, optional
            Fixed budget of the demand (in each period). The default is 1000000.
        entry_rate : float, optional
            Poisson parameter; rate of entry of new firms, i.e. the expected
            value of the number of firms entering the market in each iteration. 
            The default is 0.5.
        initial_unit_cost : numeric, optional
            Initial technology in terms of unit production cost. Initially, 
            all agents will uniformly use this technology. The default is 10.
        innovation_probability : float, optional
            Probability of success of innovative research. The default is 0.08.
        imitation_probability : float, optional
            Probability of success of imitative research. The default is 0.5.
        max_innovation_size : float, optional
            Maximum size of innovative research innovations in terms of share 
            of unit production cost reduction. Exact size will be chosen 
            randomly from a uniform between zero and max_innovation_size. 
            The default is 0.01.
        dividend_share : float, optional
            Share of revenue that is paid as dividends to shareholders and can
            this not be used by the firm in the subsequent period and more. 
            The default is 0.02.

        Returns
        -------
        None.

        """
        
        """ Record parameters"""
        self.n_agents = n_agents   
        self.t_max = t_max 
        self.demand_budget_per_period = demand_budget_per_period
        self.entry_rate = entry_rate
        self.initial_unit_cost = initial_unit_cost
        self.market_price = self.initial_unit_cost
        self.agent_parameters = {"imitation_cost": 500,
                                 "innovation_cost": 5000,
                                 "imitation_probability": imitation_probability,
                                 "innovation_probability": innovation_probability,
                                 "maximum_innovation_size": max_innovation_size,
                                 "agent_initial_capital": 10000,
                                 "innovation_success_investment": 10000,
                                 "dividend_share": dividend_share}
        
        """ Create agents"""
        self.agents_list = []
        
        for i in range(self.n_agents):
            A = Agent(self, i, self.agent_parameters, self.initial_unit_cost)
            self.agents_list.append(A)
        self.agent_idx = i + 1
        
        """ Prepare history lists"""
        self.history_t = []
        self.history_n_firms = []
        self.history_market_price = []
        self.history_market_quantity = []
        self.history_demand_fulfillment = []
        self.history_tech_frontier = []
        self.history_nhhi = []
        
    def run(self):
        """
        Run method. Handles the time iteration of the simulation.

        Returns
        -------
        None.

        """
        for t in range(self.t_max):
            
            """ Each agent allocates funds and conducts research"""
            for A in self.agents_list:
                A.research()
            
            """ Market is cleared and revenue paid"""
            self.market_clearance()
            
            """ Defaulting agents exit"""
            for A in self.agents_list:
                if A.exit():
                    self.agents_list.remove(A)
            
            """ Market entry"""
            self.entry()
            
            """ Record history variables (as far as not done in the 
                market_clearance() method.)"""
            self.history_t.append(t)
            self.history_n_firms.append(len(self.agents_list))

    
    def market_clearance(self):
        """
        Market mechanism method. Collects production offers from agents, com-
        putes the market price, handles transactions (revenue payment) with 
        active producers, and records state.

        Returns
        -------
        None.

        """
        
        """ Collect offers from agents"""
        capacities = []
        prices = []
        for A in self.agents_list:
            capacity, price = A.offer()
            capacities.append(capacity)
            prices.append(price)
        
        """ Sort offers by price"""
        idx = np.argsort(prices)

        """ Prepare variables for market clearance process"""
        cumulative_quantity = 0
        individual_revenues = []
        market_quantity = 0
        producers_at_capacity = []
        market_price = False
        
        """ Loop through cheapest producers until market is cleared"""
        for i in idx:
            quantity = capacities[i]
            if (cumulative_quantity + quantity) * prices[i] < self.demand_budget_per_period:
                """ As long as there is excess demand, just mark producer as 
                    producing at capacity"""
                market_quantity += quantity
                cumulative_quantity += quantity
                producers_at_capacity.append(i)
                market_price = prices[i]
            elif cumulative_quantity * prices[i] < self.demand_budget_per_period:
                """ When demand is exhausted, award the current producer just 
                    what is left and not covered by cheaper producers.
                    This marginal producer's cost will be the market price."""
                remaining_demand = self.demand_budget_per_period - cumulative_quantity * prices[i]
                cumulative_quantity += quantity
                quantity = remaining_demand / prices[i]
                market_price = prices[i]
                market_quantity += quantity
                
                """ Record the marginal producer's revenue."""
                A = self.agents_list[i]
                revenue = market_price * quantity
                A.obtain_revenue(revenue)
                individual_revenues.append(revenue)
        
        """ Record the revenues of all (cheaper) producers that work at 
            capacity."""
        for i in producers_at_capacity:
            A = self.agents_list[i]
            revenue = market_price * capacities[i]
            A.obtain_revenue(revenue)
            individual_revenues.append(revenue)
        
        """ Compute market shares"""
        cumulative_revenue = np.sum(individual_revenues)
        individual_revenues = np.asarray(individual_revenues) / cumulative_revenue
        
        """ Compute normalized Hirschman-Herfindahl-Index as concentration 
            measure"""
        """ Hirschman-Herfindahl-Index"""
        hhi = np.sum(individual_revenues**2)
        n = len(individual_revenues)
        """ Normalized Hirschman-Herfindahl-Index"""
        if n > 1:
            nhhi = (hhi - 1 / n) / (1 - 1 / n)
        else:
            nhhi = 1
        
        """ Record state into history variables"""
        self.history_market_price.append(market_price)
        self.history_market_quantity.append(market_quantity)
        self.history_demand_fulfillment.append(cumulative_revenue 
                                                / self.demand_budget_per_period)
        if len(prices) > 0:
            self.history_tech_frontier.append(prices[idx[0]])
        else: 
            self.history_tech_frontier.append(self.history_tech_frontier[-1])
        self.history_nhhi.append(nhhi)        
        
    def entry(self):
        """
        Method for handling market entry.

        Returns
        -------
        None.

        """
        
        """ Number of agents entering each turn follows a Poisson process."""
        n_new = np.random.poisson(self.entry_rate)
        if n_new > 0:
            """ If there are new entrants in this turn, create them, record 
                them into the agents_list, and adjust the counter variable 
                self.agent_idx"""
            for i in range(self.agent_idx, self.agent_idx + n_new):
                A = Agent(self, i, self.agent_parameters, self.market_price)
                self.agents_list.append(A)
            self.agent_idx = i + 1
    
    def imitation(self):
        """
        Method for handling imitative research frim the Simulation's side.
        Selects a random agent and returns their technology to the imitator.
        
        Returns
        -------
        tech : numeric
            Technology in terms of unit production cost.

        """
        A = np.random.choice(self.agents_list)
        _, tech = A.offer()
        return tech
    
    def return_results(self):
        """
        Method for returning and visualizing results

        Returns
        -------
        simulation_history : dict
            Recorded data on the simulation run.

        """

        """ Prepare return dict"""        
        simulation_history = {"history_t": self.history_t,
                              "history_n_firms": self.history_n_firms,
                              "history_market_price": self.history_market_price,
                              "history_market_quantity": self.history_market_quantity,
                              "history_demand_fulfillment": self.history_demand_fulfillment,
                              "history_tech_frontier": self.history_tech_frontier,
                              "history_nhhi": self.history_nhhi}

        """ Create figure showing the development of the simulation in six
            subplots."""
        fig, ax = plt.subplots(nrows=2, ncols=3, squeeze=False)
        ax[0][0].plot(self.history_t, self.history_market_quantity)
        ax[0][0].set_ylabel("Output quantity")
        #ax[0][0].set_xlabel("Time")
        ax[0][1].plot(self.history_t, self.history_demand_fulfillment)
        ax[0][1].set_ylabel("Demand fulfilled")
        #ax[0][1].set_xlabel("Time")
        ax[0][2].plot(self.history_t, self.history_n_firms)
        ax[0][2].set_ylabel("Firms")
        #ax[0][2].set_xlabel("Time")
        ax[1][0].plot(self.history_t, self.history_market_price)
        ax[1][0].set_ylabel("Market price")
        ax[1][0].set_xlabel("Time")
        ax[1][1].plot(self.history_t, self.history_tech_frontier)
        ax[1][1].set_ylabel("Technology frontier")
        ax[1][1].set_xlabel("Time")
        ax[1][2].plot(self.history_t, self.history_nhhi)
        ax[1][2].set_ylabel("norm. HHI")
        ax[1][2].set_xlabel("Time")
        
        """ Save (as pdf) and show figure"""
        plt.tight_layout()
        plt.savefig("technological_change_simulation.pdf")
        plt.show()
        
        return simulation_history
        
        
""" Main entry point"""    
if __name__ == '__main__':
    S = Simulation()
    S.run()
    results = S.return_results()
