# Example scripts for Computational Economics II

This repository contains the scripts with the example models used for Computational Economics II this winter term 2023/24. They are also available on OPAL.

In detail:
- [ ] Technology choice ABM inspired by Brian Arthur
- [ ] ABM for simulations of technological change inspired by Richard Nelson and Sidney Winter
- [ ] Macro-level business cycle simulation model based on Matheus Grasselli and Bernardo Costa-Lima (based on Richard Goodwin, Hyman Minsky, Steve Keen)
- [ ] Leontief input-output model to be initialized with data from the OECD world input-output database 2018
